"use strict";

document.addEventListener('DOMContentLoaded', () => {
    const inputTexts = document.querySelectorAll('.flexInput input[type="text"]');
    const allInputs = document.querySelectorAll('.flexInput input');
    const phonenum = document.getElementById(`phonenum`);
    const loader = document.getElementById(`loader`);
    const form = document.getElementById(`mainForm`);

    function validatePhone(phone) {
        let ukPattern = /^\+380\d{9}$/;
        return ukPattern.test(phone);
    }

    function emptyPhone(empty) {
        return empty == '';
    }

    function validateForm() {
        let error = 0;
        inputTexts.forEach((el) => {
            if (el.value == '') {
                el.style.boxShadow = 'none';
                error++;
            } else {
                el.style.boxShadow = '0rem 0rem .5rem green';
            }
        });

        if (validatePhone(phonenum.value)) {
            phonenum.style.boxShadow = '0rem 0rem .5rem green';
        } else if (!emptyPhone(phonenum.value) == '') {
            phonenum.style.boxShadow = 'none';
            error++;
        } else {
            phonenum.style.boxShadow = '0rem 0rem .5rem red';
            error++;
        }

        return error;
    }

    form.addEventListener(`submit`, submitForm);

    async function submitForm(e) {
        e.preventDefault();

        let error = validateForm();

        let formData = new FormData(form);

        if (error === 0) {
            loader.style.display = `flex`;
            let response = await fetch('./php/api.php', {
                method: 'POST',
                body: formData
            });
            if (response.ok) {
                form.reset();
                allInputs.forEach(el => {
                    el.style.boxShadow = 'none';
                });
                loader.style.display = `none`;
                window.location.href = `./php/thankyoupage.php`;
            } else {
                alert(`Ошибка отправки`);
                loader.style.display = `none`;
            }
        } else {
            allInputs.forEach(el => {
                if (el.value == '') {
                    el.style.boxShadow = '0rem 0rem .5rem red';
                }
            });
        }
    }

    inputTexts.forEach((el) => {
        el.addEventListener('input', validateForm);
    });

    phonenum.addEventListener('input', validateForm);
});