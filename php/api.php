<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = $_POST["name"];
    $city = $_POST["city"];
    $mail = $_POST["mail"];
    $phonenum = $_POST["phonenum"];

    $message_text = "Новый заказ компании AccesSense!\n\nИмя: $name\n\nГород: $city\n\nОтделение Новой Почты: $mail\n\nНомер телефона: $phonenum";

    $bot_token = '';
    $channel_id = '';

    $api_url = "https://api.telegram.org/bot$bot_token/sendMessage";

    $post_data = array(
        'chat_id' => $channel_id,
        'text' => $message_text,
    );

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $api_url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($ch);

    echo 'Отправлен запрос в Telegram API:' . PHP_EOL;
    echo 'URL: ' . $api_url . PHP_EOL;
    echo 'Данные: ' . json_encode($post_data) . PHP_EOL;
    echo 'Ответ: ' . $response . PHP_EOL;

    curl_close($ch);


    if ($response === false) {
        echo 'Ошибка cURL: ' . curl_error($ch);
    } else {
        echo 'Отправка увенчалась успехом!';
    }

} else {
    echo 'Ошибка: форма должна быть отправлена методом POST.';
}

