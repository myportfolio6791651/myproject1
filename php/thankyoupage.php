<!DOCTYPE html>
<html lang="uk">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AccesSense</title>
    <link rel="icon" href="../img/favicon_s.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/reset.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/thankyoupage.css">
</head>

<body>
    <section class="paddingSect">
        <div class="blockMainWhanks">
            <div class="blockLogoIcon">
                <img src="../img/logo2.svg" alt="icon">
            </div>
            <div class="blockHeadZakaz">
                <h2>ДЯКУЄМО ЗА ВАШЕ ЗАМОВЛЕННЯ!</h2>
            </div>
            <div class="blockManegere1">
                <div class="firstParagraph">
                    <img src="../img/icon-park-solid_one-key.svg" alt="firstParagraph">
                </div>
                <p>1. Очікуйте зв'язку з менеджером протягом доби.</p>
            </div>
            <div class="blockManegere1">
                <div class="firstParagraph">
                    <img src="../img/icon-park-solid_two-key.svg" alt="secondParagraph">
                </div>
                <p>2. Переконайтесь що введені данні є коректними.</p>
            </div>
            <div class="blockManegere1">
                <div class="firstParagraph">
                    <img src="../img/icon-park-solid_three-key.svg" alt="threeParagraph">
                </div>
                <p>3. Відправка товару відбудеться протягом доби.</p>
            </div>
        </div>
    </section>
</body>

</html>