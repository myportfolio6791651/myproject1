<!DOCTYPE html>
<html lang="uk">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description"
        content="Портативна сонячна система це ваш надійний компаньйон у всіх подорожах та ситуаціях. Забудьте про темряву та обмеження електромережі – Junai JA-2007 принесе світло та можливості туди, де це потрібно найбільше.">
    <meta name="keywords"
        content="Junai, AccesSense, сонячна система, портативна, зручність та лекгість, індивідуальний підхід, ексклюзивність продукції, JA-2007, Junai JA-2007, купити Junai, придбати Junai, технології, світло, свет, ліхтарі, сетильники, светильник, павербанк, power-bank, світильник.">
    <meta property="og:title" content="AccesSense">
    <meta property="og:description"
        content="Junai, AccesSense, сонячна система, портативна, зручність та лекгість, індивідуальний підхід, ексклюзивність продукції, JA-2007, Junai JA-2007, купити Junai, придбати Junai, технології, світло, свет, ліхтарі, сетильники, светильник, павербанк, power-bank, світильник.">
    <meta property="og:image" content="./img/main_icon.svg">
    <title>AccesSense</title>
    <link rel="icon" href="./img/favicon_s.ico" type="image/x-icon">
    <link rel="stylesheet" href="./css/reset.css">
    <link rel="stylesheet" href="./css/style.css?v=<?php echo uniqid(); ?>">
    <link rel="stylesheet" href="./php/api.php?v=<?php echo uniqid(); ?>">
</head>

<body>
    <div class="loaderPos" id="loader">
        <span class="loader"></span>
    </div>
    <dialog id="myPolicy">
        <div class="mainBlockPolicy">
            <div class="closePolicy" id="close">
                <img src="./img/carbon_close-filled.svg">
            </div>
            <h2>Політика конфіденційності AccesSense</h2>
            <p>
                1. Загальні положення
                <br>
                <br>
                1.1. Компанія AccesSense зобов'язується захищати конфіденційність ваших персональних даних та
                забезпечувати їх безпеку відповідно до чинного законодавства про захист персональних даних.
                <br>
                <br>
                1.2. Ця Політика конфіденційності пояснює, як ми збираємо, використовуємо та захищаємо ваші персональні
                дані при використанні нашого веб-сайту.
                <br>
                <br>
                2. Збір та Використання Інформації
                <br>
                <br>
                2.1. Ми можемо збирати особисту інформацію, яку ви нам надаєте під час оформлення замовлення, реєстрації
                на сайті чи іншої взаємодії з нашим веб-сайтом.
                <br>
                <br>
                2.2. Ваша інформація використовується для обробки замовлень, забезпечення доступу до особистого
                кабінету, надсилання інформаційних розсилок та покращення якості обслуговування.
                <br>
                <br>
                3. Захист та Зберігання Даних
                <br>
                <br>
                3.1. Ми застосовуємо технічні та організаційні заходи для захисту ваших персональних даних від
                несанкціонованого доступу, втрати чи руйнування.
                <br>
                <br>
                3.2. Ваша інформація зберігається лише протягом терміну, необхідного для досягнення цілей, зазначених у
                цій Політиці конфіденційності.
                <br>
                <br>
                4. Надання Даних Третім Сторонам
                <br>
                <br>
                4.1. Ми не передаємо ваші персональні дані третім сторонам без вашого згоди, за винятком випадків,
                передбачених чинним законодавством.
                <br>
                <br>
                5. Ваші Права
                <br>
                <br>
                5.1. Ви маєте право в будь-який момент вимагати доступу, виправлення чи видалення своїх персональних
                даних, а також скасування вашої згоди на їх обробку.
                <br>
                <br>
                5.2. Для реалізації ваших прав або вирішення питань щодо конфіденційності, звертайтеся за контактними
                даними, вказаними на нашому веб-сайті.
                <br>
                <br>
                6. Зміни в Політиці конфіденційності
                <br>
                <br>
                6.1. Ми можемо оновлювати цю Політику конфіденційності час від часу. Будь ласка, регулярно перевіряйте
                її для ознайомлення зі змінами.
                <br>
                <br>
                Ця Політика конфіденційності в останній версії була оновлена 27.11.2023.
            </p>
        </div>
    </dialog>
    <header>
        <nav>
            <div class="logo">
                <a href="index.html">
                    <img src="./img/logo.svg" alt="mainLogo">
                </a>
            </div>
            <ul>
                <li><a href="#forUs">ПРО НАС</a></li>
                <li><a href="#product">ПРОДУКТ</a></li>
                <li><a href="#aspects">АСПЕКТИ</a></li>
            </ul>
            <div class="header-panel">
                <div class="panel-button" id="toggleButton">
                    <div class="menu"></div>
                    <div class="menu"></div>
                    <div class="menu"></div>
                </div>
                <ul class="dropdown-list" id="dropdownList">
                    <li><a href="#forUs" class="styleLink">ПРО НАС</a></li>
                    <li><a href="#product" class="styleLink">ПРОДУКТ</a></li>
                    <li><a href="#aspects" class="styleLink">АСПЕКТИ</a></li>
                    <li><a href="#orderBlock" class="styleLink">ЗАМОВИТИ</a></li>
                </ul>
            </div>
            <div class="orderBlock">
                <p><a href="#orderBlock">ЗАМОВИТИ</a></p>
            </div>
        </nav>
        <div class="headDescription">
            <div class="wrraperHeadDescription">
                <h2>AccesSense</h2>
                <p class="mainTextDescription">Зручність та Технологічність через Унікальний Досвід Покупок</p>
                <p class="secondTextDescription">Ласкаво просимо в світ AccesSense, де ми не просто продаємо, а
                    створюємо власний вимір стилю,
                    зручності та технологічності. Ми не тільки продавці - ми ваші гіди в світі елегантності,
                    індивідуальності та інноваційних технологій. Приєднуйтеся до нас у подорожі, де кожна покупка стає
                    особистим вираженням вашого неповторного смаку.</p>
            </div>
        </div>
    </header>
    <section>
        <div class="blockForUs" id="forUs">
            <div class="paddingBlocks">
                <div class="headTextForUs">
                    <h2>ПРО НАС</h2>
                    <p class="mainTextForUs">AccesSense – це не просто бренд, але справжній компаньйон для тих,
                        хто цінує високу якість, стиль, комфорт високу технологічність у своєму повсякденному житті . Ми
                        пропонуємо не тільки продукти, але і унікальний досвід покупок, що об'єднує нас</p>
                </div>
                <div class="wrraperForUsBlock">
                    <div class="bgBlock">
                        <div class="mainBlocksInfo">
                            <div class="imgBlocks">
                                <img src="./img/look.webp" alt="look1">
                            </div>
                            <p class="forUsHead">
                                Індивідуальний Підхід
                            </p>
                            <p class="forUsTextMain">
                                Ми розуміємо, що стиль, комфорт та відношення до техніки – це особиста справа. Наша
                                продукція вражає не лише відмінною якістю, але й унікальними цінами та індивідуальним
                                підходом до вибору продукту.
                            </p>
                        </div>
                    </div>
                    <div class="bgBlock">
                        <div class="mainBlocksInfo">
                            <div class="imgBlocks">
                                <img src="./img/look2.webp" alt="look2">
                            </div>
                            <p class="forUsHead">
                                Зручність та Легкість
                            </p>
                            <p class="forUsTextMain">
                                AccesSense пропонує не тільки стильні рішення, але й комфорт та високу технологічність,
                                дозволяючи вам більше часу приділяти собі і не відмовляти собі в жодному задоволенні.
                            </p>
                        </div>
                    </div>
                    <div class="bgBlock">
                        <div class="mainBlocksInfo">
                            <div class="imgBlocks">
                                <img src="./img/look3.webp" alt="look3">
                            </div>
                            <p class="forUsHead">
                                Ексклюзивність Продукції
                            </p>
                            <p class="forUsTextMain">
                                Ми пишаємося тим, що пропонуємо вам продукцію від відомих брендів, які не лише
                                визначають тренди, але й втілюють у собі елегантність, стиль, високу якість продукції та
                                комфорт.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="blockProduct" id="product">
            <div class="paddingBlocks">
                <div class="headTextForUs">
                    <h2>ПРОДУКТ</h2>
                    <p class="mainTextForUs">На сьогоднішній день до нас надходять великі партії товару, який повинен
                        бути у кожному домі у будь-яку хвилину. Це дозволяє Вам світити не тільки своєю посмішкою, але і
                        портативною сонячною системою Junai JA-2007</p>
                </div>
                <div class="wrraperProductFirstBlock">
                    <div class="mainImgBlockProduct">
                        <div class="imgBlockProd">
                            <img src="./img/product1.svg" alt="product1">
                        </div>
                    </div>
                    <div class="wrraperInfoBlocks">
                        <div class="mainBlockInfoProd">
                            <div class="bulbIcon">
                                <img src="./img/tabler_bulb.svg" alt="bulb">
                            </div>
                            <div class="mainTextBlockProd">
                                <p>
                                    <span>Junai JA-2007</span> – це не просто портативна сонячна система, це ваш
                                    надійний компаньйон
                                    у всіх подорожах та ситуаціях. Забудьте про темряву та обмеження електромережі –
                                    Junai JA-2007 принесе світло та можливості туди, де це потрібно найбільше.
                                </p>
                            </div>
                        </div>
                        <div class="secondBlockInfoText">
                            <div class="informationForProd">
                                <div class="numberIcon">
                                    <img src="./img/icon-park-solid_one-key.svg" alt="firstIcon">
                                </div>
                                <p><span>Сонячна Автономія:</span>
                                    Завдяки вбудованій сонячній батареї, Junai JA-2007 забезпечує енергією навіть у
                                    відсутність електрики. Це робить його ідеальним варіантом для виїзду на природу,
                                    кемпінгу та у випадку відключення електропостачання.</p>
                            </div>
                            <div class="informationForProd">
                                <div class="numberIcon">
                                    <img src="/img/icon-park-solid_two-key.svg" alt="secondIcon">
                                </div>
                                <p><span>Портативність та Зручність:</span><br>
                                    Легка та компактна конструкція Junai JA-2007 робить її ідеальною для мандрівок та
                                    поїздок. Завжди маєте при собі надійне джерело світла.</p>
                            </div>
                            <div class="informationForProd">
                                <div class="numberIcon">
                                    <img src="/img/icon-park-solid_three-key.svg" alt="fthreeIcon">
                                </div>
                                <p><span>Додаткові Лампочки:</span> <br> Оснащена двома додатковими лампочками, що
                                    розширюють можливості
                                    використання. Це не лише джерело освітлення, але і допоміжний елемент в надзвичайних
                                    ситуаціях.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wrraperProductSecondBlock">
                    <div class="secondWrraperInfoBlocks">
                        <div class="secondBlockInfoText marginTop">
                            <div class="secondInformationForProd">
                                <div class="numberIcon">
                                    <img src="/img/icon-park-solid_four-key.svg" alt="fourIcon">
                                </div>
                                <p><span>Power Bank Функція:</span><br>
                                    Не тільки світло, але й зручний Power Bank для заряджання вашого смартфону, планшету
                                    чи ноутбука. Подаруйте своїм пристроям невичерпну енергію в будь-якому місці.</p>
                            </div>
                            <div class="secondInformationForProd">
                                <div class="numberIcon">
                                    <img src="/img/icon-park-solid_five-key.svg" alt="fiveIcon">
                                </div>
                                <p><span>Довгий Час Роботи:</span> <br>Акумулятор Junai JA-2007 може працювати до 20
                                    годин залежно від рівня яскравості освітлення, забезпечуючи стабільне та тривале
                                    використання.</p>
                            </div>
                        </div>
                    </div>
                    <div class="wrrapersImgSecondBlocks">
                        <div class="secondBlockImgProduct">
                            <img src="./img/product2.svg" alt="product1">
                        </div>
                        <div class="secondBlockImgProduct">
                            <img src="./img/product3.svg" alt="product1">
                        </div>
                        <div class="secondBlockImgProduct">
                            <img src="./img/product4.svg" alt="product1">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="blocksAspects" id="aspects">
            <div class="headTextForUs">
                <h2>АСПЕКТИ</h2>
                <p class="mainTextForUs">Наша гордість цього продукту - в тому, що він саме зараз може поліпшити ваше
                    життя та задовольнити кожну хвилину вашого перебування деінде</p>
            </div>
            <div class="blocksPaddingAspects">
                <div class="mainBLockAspects">
                    <div class="blockPlus">
                        <h3>ПЛЮСИ</h3>
                    </div>
                    <div class="wrraperPlusAspects">
                        <div class="mainblockAscpects">
                            <div class="iconAscpects">
                                <img src="./img/lets-icons_done-ring-round.svg" alt="iconPlus">
                            </div>
                            <p class="forUsHead headFont">
                                Екстрений Випадок:
                            </p>
                            <p class="forUsTextMain">
                                Junai JA-2007 - ваш надійний союзник у випадку воєнних дій чи надзвичайних ситуацій.
                                Забезпечте себе світлом та зарядженням навіть під час обмежень енергопостачання.
                            </p>
                        </div>
                        <div class="mainblockAscpects">
                            <div class="iconAscpects">
                                <img src="./img/lets-icons_done-ring-round.svg" alt="iconPlus">
                            </div>
                            <p class="forUsHead headFont">
                                Кемпінг та Подорожі:
                            </p>
                            <p class="forUsTextMain">
                                Ідеальний вибір для тих, хто цінує незалежність та комфорт у подорожах. Junai JA-2007
                                додає енергії та атмосфери зручності.
                            </p>
                        </div>
                        <div class="mainblockAscpects">
                            <div class="iconAscpects">
                                <img src="./img/lets-icons_done-ring-round.svg" alt="iconPlus">
                            </div>
                            <p class="forUsHead headFont">
                                Відключення Електромережі:
                            </p>
                            <p class="forUsTextMain">
                                Збережіть звичайний ритм життя і зробіть його комфортнішим навіть при відключенні
                                електропостачання.
                            </p>
                        </div>
                    </div>
                    <div class="blockPlus">
                        <h3>МІНУСИ</h3>
                    </div>
                    <div class="wrraperPlusAspects">
                        <div class="mainblockAscpects borderRed">
                            <div class="iconAscpects">
                                <img src="./img/bx_error.svg" alt="iconPlus">
                            </div>
                            <p class="forUsHead headFont">
                                Залежність від Сонця:
                            </p>
                            <p class="forUsTextMain">
                                Для повноцінної роботи потребує сонячного світла, тому в умовах безсонячного періоду
                                може зменшувати продуктивність.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="orderBlocks" id="orderBlock">
            <div class="paddingBlocks">
                <div class="headTextForUs">
                    <h2>ЗАМОВИТИ</h2>
                    <p class="mainTextForUs">Замовляёте Junai JA-2007 прямо зараз, та оберіть шлях свтлої сторони прямо
                        зараз!</p>
                </div>
                <div class="orderBgForm">
                    <form id="mainForm">
                        <div class="blockIconSelected">
                            <img src="./img/material-symbols_order-approve.svg" alt="iconSelected">
                        </div>
                        <p class="textForm">
                            Щоби стати власником портативної сонячної системи, заповніть форму нижче, та очікуйте
                            зв’язку з менеджером!
                        </p>
                        <p class="pricePolicy">
                            Ціна: 1200 грн.
                        </p>
                        <div class="flexInput">
                            <div class="inputbox">
                                <input type="text" name="name" id="name" placeholder="Ваше ім’я та прізвище">
                            </div>
                            <div class="inputbox">
                                <input type="text" name="city" id="city" placeholder="Місто куди надсилати">
                            </div>
                            <div class="inputbox">
                                <input type="text" name="mail" id="mail" placeholder="Відділення нової пошти">
                            </div>
                            <div class="inputbox">
                                <input type="tel" name="phonenum" id="phonenum" placeholder="Номер телефону">
                                <p>* Приклад корректного номеру: +380XXXXXXXXX</p>
                            </div>
                            <button type="submit" name="submitBtn" id="submitBtn">ЗАМОВИТИ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="logo">
            <a href="#">
                <img src="./img/logo.svg" alt="mainLogo">
            </a>
        </div>
        <div class="privacyPolicy">
            <p id="modal_window">ПОЛІТИКА КОНФІДЕНЦІЙНОСТІ</p>
        </div>
        <div class="date">
            <p>© 2023</p>
        </div>
    </footer>
    <script src="./js/dropdown.js"></script>
    <script src="./js/privacyPolicy.js"></script>
    <script src="./js/validator.js?v=<?php echo uniqid(); ?>"></script>
</body>

</html>